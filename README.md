# My Personal Productivity Suite

I have always been dissatisfied with the proprietary productivity tools, so I decided to create [my own](https://www.efficient.place).

### Components

#### TODO App [v1.0]
* [x] progress bars for medium and long term tasks
* [x] organisation of tasks by tags with filtering function 
* [x] sorting, marking for completion and deletion functions

#### Timetable App [v2.0]
* [x] presented as a webpage, with easily management of tasks at the UI
* [x] viewing of previous tables, organised by date

#### Home page [v3.0]
* [x] Create a personal webpage with a search bar 
* [x] other tools should be accessible from the page
* [x] hosted on heroku
* [x] customisable homepage links for each user
* [x] buying a domain for easier access

#### Notetaker [v4.0]
* [x] Integrate my current note taking workflow into the suite to be able to be read readily accessible from homepage
* [x] tools to create, view and edit notes
* [x] preview / viewing markdown notes during editing notes
* [x] Encryption of user notes before saving to database

#### Polish and improvements [v5.0](pending)
* TODO app: calendar integration
* Timetable app: integration with TODO app
* Notetaker: tweaks to functionality
