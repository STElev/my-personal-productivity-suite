async function fileInput() {
    let data = document.querySelector("#text-input").value;
    let filename = document.querySelector("#filename-input").value.trim();
    let tagSelect = document.querySelector("#tag-selector").value;
    let catSelect = document.querySelector("#cat-selector").value;
    let catInput = document.querySelector("#category-field").value.trim();
    let tagInput = document.querySelector("#tag-field").value.trim();

    let data_logic = {
        "new-tag": catSelect + "/" + tagInput,
        "new-cat-tag": catInput.trim() + "/" + tagInput
    };
    if (tagSelect !== "new-tag" && tagSelect !== "new-cat-tag") {
        tag = tagSelect;
    } else {
        tag = data_logic[tagSelect];
    }
    if (!tag || !data || !filename) {
        alert("Error: Empty field. Cannot save note");
        flash("Error: Empty field. Cannot save note", "is-warning");
        return;
    }
    if (!checkANS(filename) || !checkANS(tag.split('/')[0]) || !checkANS(tag.split('/')[0])) {
        alert("Error: Special characters are not allowed as filename or tag");
        flash("Error: Special characters are not allowed as filename or tag", "is-warning");
        return;
    }
    
    let filePath = tag + '/' + filename; 
    data = await encrypt(data);
    let userFile = new File([data], filePath, { type: 'plain/text' });
    getSignedRequest(userFile);
    return;
}

function checkANS(str){
    if (str.match(/^[a-z 0-9]+$/i)){
        return true;
    } else
        return false;
}

function getSignedRequest(file){
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/sign_s3_post?file_name="+file.name+"&file_type="+file.type);
  xhr.onreadystatechange = function(){
    if(xhr.readyState === 4){
      if(xhr.status === 200){
        var response = JSON.parse(xhr.responseText);
        uploadFile(file, response.data, response.url);
      }
      else{
        flash("Could not get signed URL", "is-warning");
      }
    }
  };
  xhr.send();
}
function uploadFile(file, s3Data, url){
    let saveBtn = document.querySelector("#save-button");
    saveBtn.classList.add("is-loading");
    var xhr = new XMLHttpRequest();
    xhr.open("POST", s3Data.url);
    var postData = new FormData();
    for(key in s3Data.fields){
        postData.append(key, s3Data.fields[key]);
    }
    postData.append('file', file);
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            saveBtn.classList.remove("is-loading");
            flash("Changes saved successfully", "is-success");
        }
    };
    xhr.send(postData);
}


function getNote() {
    var xhr = new XMLHttpRequest();
    let fileName = document.querySelector('#note-selector').value;
    fileName = fileName.replaceAll('+', ' ');
    xhr.open("GET", "/get_download_link/" + fileName);
    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            if(xhr.status >= 200 || xhr.status < 300){
                var response = xhr.responseText;
                downloadNote(response);
            }
            else {
                flash("Could not get signed URL", "is-warning");
                return;
            }
        }
    }
    xhr.send();
}

function downloadNote(url) {
    let fileName = document.querySelector('#note-selector').value;
    let xhr = new XMLHttpRequest();
    let viewer = document.querySelector('#text-input');
    let viewerLoader = document.querySelector('#note-loader');
    viewerLoader.classList.add("is-loading");
    xhr.open("GET", url);
    xhr.onreadystatechange = async function(){
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                try {
                    var response = xhr.response;
                    viewer.value = await decrypt(response);
                    viewerLoader.classList.remove("is-loading");
                    viewer.removeAttribute('readonly');
                    displayTitle(fileName);
                    convert();
                } catch (err) {
                    console.log("Error in decrypting note");
                    flash("Error: cannot decrypt selected note!", "is-danger");
                    viewerLoader.classList.remove("is-loading");
                }
            } else {
                console.log("Cannot retrieve note");
                flash("Error: cannot retrieve selected note!", "is-danger");
                viewerLoader.classList.remove("is-loading");
            }
        };
    }
    xhr.send();
}

function displayTitle(fileName){
    let file = fileName.slice(fileName.lastIndexOf('/') + 1, fileName.length);
    let tag = fileName.slice(fileName.indexOf('/') + 1, fileName.lastIndexOf('/'));
    let titleField = document.querySelector("#title");
    let pathField = document.querySelector("#path");
    while (titleField.firstChild || pathField.firstChild){
        titleField.removeChild(titleField.firstChild);
        pathField.removeChild(pathField.firstChild);
    }

    let title = document.createElement('h');
    let path = document.createElement('h');
    title.appendChild(document.createTextNode(formatTitle(file)));
    path.appendChild(document.createTextNode(formatTitle(tag)));

    title.classList.add('title');
    title.classList.add('is-4');
    path.classList.add('subtitle');
    path.classList.add('is-6');
    titleField.appendChild(title);
    pathField.appendChild(path);
    
    document.querySelector("#tag-selector").value = formatTitle(tag);
    document.querySelector("#filename-input").value = formatTitle(file);
    return;
}

function formatTitle(text){
    return decodeURIComponent(text).replaceAll('+', ' ');
}

function requestList(){
    return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/user_note_list/");

    xhr.onload = () => {
            if(xhr.status >= 200 && xhr.status < 300){
                let list = JSON.parse(xhr.responseText);
                resolve(list);
            }
            else {
                reject(xhr.responseText);
            }
        };
    xhr.onerror = function () {
      reject({
        status: xhr.status,
        statusText: xhr.statusText
      });
    };
    xhr.send();
});
}

function populateList(list, value_list, id, loaderid){
    var field = document.querySelector(id);
    var loader = document.querySelector(loaderid);
    for(let i = 0; i < list.length; i++) {
        field.options[field.options.length] = new Option(value_list[i], list[i]);
    }
    loader.classList.remove("is-loading");
}

function chooseTag(){
    let tagChoice = document.querySelector("#tag-selector").value;
    if (tagChoice !== 'new-tag' && tagChoice !== 'new-cat-tag'){
        tagChoice = 'other';
    }
    let catControl = document.querySelector("#cat-control");
    let catInput = document.querySelector("#category-input");
    let tagInput = document.querySelector("#tag-input");
    
    let choices = {
        'new-tag': ()=>{
            show(catControl);
            hide(catInput);
            show(tagInput);
        },
        'new-cat-tag': ()=>{
            hide(catControl);
            show(catInput);
            show(tagInput);
        },
        'other': ()=>{
            hide(catControl);
            hide(catInput);
            hide(tagInput);
        }
    };
    choices[tagChoice]();
}

function convert(){
    let converter = new showdown.Converter({
        tables: true,
        tasklists: true,
        strikethrough: true,
        simpleLineBreaks: true,
    });
    let ta = document.querySelector("#text-input");
    let res = document.querySelector("#text-output");
    let html = converter.makeHtml(ta.value);
    ta.removeAttribute('style');
    let replaceList = {
        "text-align:center": "text-align:left",
    };
    for (key in replaceList){
        html = html.replaceAll(key, replaceList[key]);
    }
    res.innerHTML = HtmlSanitizer.SanitizeHtml(html);
    if (res.offsetHeight !== 0){
        ta.style.height = res.offsetHeight + 'px';
    }
}

async function list_notes(){
    try {
        const response = await requestList();
        let notes = response["notes"];
        notes_formatted = [];
        for(let i = 0; i < notes.length; i++) {
            notes[i] = decodeURIComponent(notes[i]);
            let item = notes[i].replace(notes[i].split('/')[0],'').replaceAll('+',' ').replace('/', '').replaceAll('/', ' / ');
            notes_formatted = notes_formatted.concat(item);
        }
        populateList(notes, notes_formatted, "#note-selector", "#note-loader");
    } catch(err) {
        console.log("Cannot retrieve notes. Could be that there are none saved yet.");
        document.querySelector("#note-loader").classList.remove("is-loading");
    }
}

async function list_tags(){
    try {
        const response = await requestList();
        let tags = response["tags"];
        let categories = response["categories"];
        let paths = response["paths"];
        populateList(paths, paths, "#tag-selector", "#tag-loader");
        populateList(categories, categories, "#cat-selector", "#cat-loader");
    } catch(err) {
        console.log("Cannot retrieve tags. Could be that there are none created yet.");
        document.querySelector("#tag-loader").classList.remove("is-loading");
        document.querySelector("#cat-loader").classList.remove("is-loading");
    }
}



function clearField(){
    document.querySelector("#text-input").value = "";
    return;
}

function changeView(){
    let toggle;
    let editor = document.querySelector("#viewer-container");
    let previewer = document.querySelector("#text-output"); 
    
    if(document.querySelector("#plaintext").checked){
        toggle = "plaintext";
    } else if(document.querySelector("#markdown").checked){
        toggle = "markdown";
    } else if(document.querySelector("#split").checked){
        toggle = "split";
    }
    
    let data = {
        "plaintext": ()=>{
            full(editor);
            full(previewer);
            show(editor);
            hide(previewer);
        },
        "markdown": ()=>{
            full(editor);
            full(previewer);
            hide(editor);
            show(previewer);
        },
        "split": ()=>{
            half(editor);
            half(previewer);
            show(editor);
            show(previewer);
        },
    };
    data[toggle]();
    convert();
    if (editor.offsetHeight === 0 && previewer.offsetHeight < 30){
        previewer.style.height = '384px';
    } else {
        previewer.removeAttribute('style');
    }
}

function show(element){element.classList.remove("is-hidden");}
function full(element){
    element.classList.remove("is-half");
//    if (element.offsetHeight < 30){
//        element.style.height = 384;
//    }
}
function hide(element){
    if(!element.classList.contains("is-hidden")){
    element.classList.add("is-hidden");
    }
}
function half(element){
    if(!element.classList.contains("is-half")){
        element.classList.add("is-half");
    }
}
