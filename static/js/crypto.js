let crypto = window.crypto;
const enc = new TextEncoder();
const dec = new TextDecoder();

let request = new Promise((resolve, reject)=>{
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/get_user_key/");
    xhr.onload = ()=>{
        if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.responseText);
            } else {
                reject(xhr.statusText);
            }
        };
    xhr.send();
});

async function getKey(){
    try {
    let res = await request;
    return await importMyKey(_base64ToArrayBuffer(res));
    } catch (err) {
        console.log("Failed to get encryption key");
    }
}

const myKey = getKey();

async function importMyKey(myKey){
    return await crypto.subtle.importKey(
        "raw",
        myKey,
        "AES-GCM",
        true,
        ["encrypt", "decrypt"] 
    );
}

function base64ToUint8Array(base64) {
    let buf = atob(base64).split(',');
    for (let i = 0; i < buf.length; i++) {
        buf[i] = Number(buf[i]);
    }
    return new Uint8Array(buf)
}

function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

async function encrypt(message){
    let iv = crypto.getRandomValues(new Uint8Array(12));
    const key = await myKey;
    ct = await crypto.subtle.encrypt(
        {name: "AES-GCM", iv: iv},
        key,
        enc.encode(message)
    );
    return new Promise((resolve, reject)=>{
        if (ct){
            resolve(btoa(new Uint8Array(ct)) + '-' + btoa(iv));
        } else {
            reject("Failed to encrypt note");
        }
    });
}

async function decrypt(responseBuffer){
    const key = await myKey;
    let ciphertext = responseBuffer.split('-')[0];
    let iv = responseBuffer.split('-')[1];

    pt = await crypto.subtle.decrypt(
        {name: "AES-GCM", iv: base64ToUint8Array(iv)},
        key,
        base64ToUint8Array(ciphertext)
    );
    return new Promise((resolve, reject)=>{
        if (pt){
        resolve(dec.decode(pt));
        } else {
        reject("Failed to decrypt note");
        }
    });
}


