from flask import Flask, render_template, redirect, request, session, url_for, flash
from flask_session import Session
from flask_talisman import Talisman
from datetime import date, datetime, timedelta
from time import time
from werkzeug.security import check_password_hash, generate_password_hash
from Crypto import Random, Protocol, Cipher, Util, Hash
from Crypto.Cipher import AES
from base64 import b64encode, b64decode
import psycopg2
import psycopg2.extras
import os
import boto3, botocore
import json
from helper import render_todos, login_required, get_deadline, get_start,get_remain_in_list, \
render_time, get_duration, render_timetable, check_overlap, set_default, render_bookmark,\
filter_duplicate, generate_new_edek, decrypt_edek

app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = os.urandom(255)
app.config['SESSION_TYPE'] = "filesystem"
PERMANENT_SESSION_LIFETIME = timedelta(days=50)
Session(app)
talisman = Talisman(app, content_security_policy=[])

DATABASE_URL = os.environ['DATABASE_URL']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
conn = psycopg2.connect(DATABASE_URL, sslmode='require')
S3_BUCKET = 'efficiency-studio'
app.config['S3_LOCATION'] = f'http://{S3_BUCKET}.s3.amazonaws.com/'
s3 = boto3.client(
   "s3",
   aws_access_key_id=AWS_ACCESS_KEY_ID,
   aws_secret_access_key=AWS_SECRET_ACCESS_KEY
)

def DEFAULT_TAGS():
    return ['household', 'finance', 'work', 'hobby']
def GET_USERNAME():
    try:
        return session["user_name"]
    except:
        return None


DEFAULT_BOOKMARKS = [
        {'name': 'Information', 
            'bookmarks': [],
            'folders': [
                {'name': 'Education',
                    'bookmarks': [
                        {'title': 'Wikipedia', 'url': 'https://wikipedia.org'},
                        {'title': 'Coursera', 'url': 'https://www.coursera.org'},
                        {'title': 'Khan Academy', 'url': 'https://www.khanacademy.org'},
                        ],
                    },
                {'name': 'News Outlet',
                    'bookmarks': [
                        {'title': 'The Guardian', 'url': 'https://theguardian.com/international'},
                        {'title': 'CNN', 'url': 'https://www.cnn.com'},
                        ],
                    },
                ],
            },
        {'name': 'Popular sites',
            'bookmarks': [
                        {'title': 'Youtube', 'url': 'https://youtube.com'},
                        {'title': 'Reddit', 'url': 'https://reddit.com'},
                        {'title': 'Amazon', 'url': 'https://amazon.com'},
                        ],
            'folders': [],
            }
        ]


@app.route("/")
def index():
    return redirect("/home") 

@app.route("/home")
def homepage():
    if GET_USERNAME():
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("SELECT * FROM bookmarks WHERE user_id = %s", [session["user_id"]])
            bookmarks = cur.fetchall()
        return render_template("home.html", username=GET_USERNAME(), page="user", bookmarks=render_bookmark(bookmarks))
    else:
        return render_template("home.html", page="user", user="false", bookmarks=DEFAULT_BOOKMARKS)

@app.route("/home/edit/", methods=["GET","POST"])
@login_required
def edit_bookmark():
    if request.method == "GET":
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("SELECT * FROM bookmarks WHERE user_id = %s", [session["user_id"]])
            bookmarks = cur.fetchall()
        return render_template("home.html", username=GET_USERNAME(), page="edit", bookmarks=render_bookmark(bookmarks))
    else:
        bookmark_delete = request.form.get("bookmark-id")
        folder_delete = request.form.get("folder-del")
        folder_category = request.form.get("folder-category")
        category_delete = request.form.get("category-del")

        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            if bookmark_delete:
                cur.execute("DELETE FROM bookmarks WHERE user_id = %s AND id = %s", (session["user_id"], bookmark_delete))
                flash("Bookmark deleted")
            elif folder_delete:
                cur.execute("DELETE FROM bookmarks WHERE user_id = %s AND folder = %s AND category = %s", (session["user_id"], folder_delete, folder_category))
                flash("Folder deleted")
            elif category_delete:
                cur.execute("DELETE FROM bookmarks WHERE user_id = %s AND category = %s", (session["user_id"], category_delete))
                flash("Category deleted")
            conn.commit()
        return redirect("/home/edit/")

@app.route("/home/create/", methods=["GET", "POST"])
@login_required
def add_bookmark():
    if request.method == "GET":
        user_cat = []
        user_fold = []
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("SELECT DISTINCT category FROM bookmarks WHERE user_id = %s", [session["user_id"]])
            for cat in cur:
                user_cat.append(cat[0])
            cur.execute("SELECT DISTINCT folder, category FROM bookmarks WHERE user_id = %s and folder != 'default'", [session["user_id"]])
            for fold in cur:
                user_fold.append((fold[0], fold[1].replace(' ', '-')))
        return render_template("home.html", username=GET_USERNAME(), page="create", categories=user_cat, folders=user_fold)
    else:
        bookmark_title = request.form.get("title")
        bookmark_url = request.form.get("url")
        category_choice = request.form.get("category-choice")
        folder_choice = request.form.get("folder-choice")

        category_choice = request.form.get("category-name") if \
                (category_choice == "create") else category_choice
        folder_choice = request.form.get("folder-name") if \
                (folder_choice == "create") else folder_choice

        if bookmark_url.find('.') < 1 or bookmark_url.endswith('.'):
            return apology("ERROR: Invalid address")
        elif not bookmark_title or not bookmark_url or not category_choice or \
                not folder_choice:
                    return apology("ERROR: Missing information")
        elif len(bookmark_title) > 20 or len(category_choice) > 20 or\
                len(folder_choice) > 20 or len(bookmark_url) > 255:
                    return apology("Error: Input too long")

        if ('https://' not in bookmark_url) and ('http://' not in bookmark_url):
            bookmark_url = 'https://' + bookmark_url
        with conn:
            cur = conn.cursor()
            cur.execute("INSERT INTO bookmarks (user_id, title, url, category, folder)\
                    VALUES (%s, %s, %s, %s, %s)", (session["user_id"], bookmark_title, \
                    bookmark_url, category_choice, folder_choice))
        return redirect("/home")

@app.route("/home/restore/")
@login_required
def restore_default():
    with conn:
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("DELETE FROM bookmarks WHERE user_id = %s", [session["user_id"]])
        bookmarks = set_default(DEFAULT_BOOKMARKS)
        for bookmark in bookmarks:
            bookmark.insert(0, session["user_id"])
        sql = "INSERT INTO bookmarks (user_id, title, url, folder, category) VALUES %s"
        psycopg2.extras.execute_values(cur, sql, bookmarks)
        conn.commit()
        flash("Restored default bookmarks")
    return redirect("/home")

@app.route("/register", methods=["GET", "POST"])
def register():
    session.clear()
    if request.method =="POST":
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            username = request.form.get("username")
            password = request.form.get("password")
            confirm = request.form.get("confirm")
            default_bookmarks = set_default(DEFAULT_BOOKMARKS)
            if not username or not password:
                return apology("ALL FIELDS MUST BE FILLED IN")

            cur.execute("select * from users where username = %s", [username])
            rows = cur.fetchall()
            if len(rows) == 1:
                return apology("USERNAME TAKEN") 
            elif password != confirm:
                return apology("PASSWORDS DO NOT MATCH")
            password_hash = generate_password_hash(password, "sha256", 2)

            # bit to generate and encrypt the DEK and store into user's DB
            nonce = Random.get_random_bytes(12)
            salt = Random.get_random_bytes(13)
            DEK = Random.get_random_bytes(32)
            EDEK = generate_new_edek(DEK, password, salt, nonce)
            cur.execute("INSERT INTO users (username, password_hash, edek) VALUES (%s, %s, %s)", (username, password_hash, EDEK))
            cur.execute("SELECT id FROM users WHERE username = %s", [username])
            user_id = cur.fetchone()[0]
            for bookmark in default_bookmarks:
                bookmark.insert(0, user_id)
            # TODO: add optional option for users to add default BMs on register
            # sql = "INSERT INTO bookmarks (user_id, title, url, folder, category) VALUES %s"
            # psycopg2.extras.execute_values(cur, sql, default_bookmarks)
            # conn.commit()
            flash("Successfully registered a new account")
            return render_template("login.html")
    else:
        return render_template("register.html", username=GET_USERNAME())

@app.route("/change", methods=["GET", "POST"])
@login_required
def change():
    if request.method == "POST":
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            old = request.form.get("password-old")
            password = request.form.get("password-new")
            confirmation = request.form.get("password-confirm")
            cur.execute("SELECT password_hash, edek FROM users WHERE id = %s", [session["user_id"]])
            rows = cur.fetchall()[0]
            password_hash = rows["password_hash"]
            edek_string = rows["edek"]
            salt = b64decode(edek_string.split('-')[0])
            nonce = b64decode(edek_string.split('-')[1])
            edek = b64decode(edek_string.split('-')[2])
            
            new_nonce = Random.get_random_bytes(12)
            new_salt = Random.get_random_bytes(13)

            dek = decrypt_edek(salt, nonce, edek, old)
            new_edek = generate_new_edek(dek, password, new_salt, new_nonce)

            if not old or not password or not confirmation:
                return apology("ALL FIELDS MUST BE FILLED IN")
            elif password != confirmation:
                return apology("PASSWORDS DO NOT MATCH")
            elif not check_password_hash(password_hash, old):
                return apology("INCORRECT PASSWORD")
            else:
                cur.execute("UPDATE users SET password_hash = %s, edek = %s WHERE id = %s", [generate_password_hash(password, "sha256", 2), new_edek, session["user_id"]])
                conn.commit()
                return redirect("/")
    else:
        return render_template("change_password.html", username=GET_USERNAME())


@app.route("/login", methods=["GET", "POST"])
def login():
    session.clear()
    if request.method == "POST":
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            username = request.form.get("username")
            cur.execute("SELECT * FROM users WHERE username = %s", [username])
            password = request.form.get("password")
            rows = cur.fetchall()
            if len(rows) != 1 or not check_password_hash(rows[0]["password_hash"], request.form.get("password")):
                return apology("INVALID USERNAME AND/OR PASSWORD")
            next_url = request.form.get("next")
            session["user_id"] = rows[0]["id"]
            session["user_name"] = rows[0]["username"]
            edek_string = rows[0]["edek"]
            salt = b64decode(edek_string.split('-')[0])
            nonce = b64decode(edek_string.split('-')[1])
            edek = b64decode(edek_string.split('-')[2])
            session["dek"] = decrypt_edek(salt, nonce, edek, password)

            cur.execute("SELECT * FROM tags WHERE user_id = %s", [session["user_id"]])
            session["user_tags"] = DEFAULT_TAGS()
            rows = cur.fetchall()
            for row in rows:
                session["user_tags"].append(row["tag"])
            flash("Successful Login")
            if not next_url:
                return redirect("/")
            return redirect(next_url)
    else:
        return render_template("login.html", username=GET_USERNAME())
@app.route("/logout")
@login_required
def logout():
    session.clear()
    return redirect("/")

@app.route("/todo")
@login_required
def todo():
    return redirect("/todo/task/deadline")

@app.route("/todo/<page>/<sorter>", methods=["GET","POST"])
@login_required
def task(sorter, page):
    if request.method == "POST":
        with conn:
            cur = conn.cursor()
            delete_id = request.form.get("delete")
            completed_id = request.form.get("completed")
            change_id = request.form.get("change_id")
            change_date = request.form.get("change_date")

            if delete_id:
                cur.execute("DELETE FROM todos WHERE id = %s", [delete_id])
                flash("Task deleted")
                return redirect(f"/todo/{page}/{sorter}")
            elif completed_id:
                cur.execute("UPDATE todos SET completed = true WHERE id = %s", [completed_id])
                conn.commit()
                flash("Task marked as completed")
                return redirect(f"/todo/{page}/{sorter}")
            elif change_date:
                if (date.fromisoformat(change_date) - date.today()).days < 0:
                    return apology("DEADLINE PRECEDES CURRENT DATE")
                cur.execute("UPDATE todos SET deadline = %s WHERE id = %s", (change_date, change_id))
                conn.commit()
                flash("Deadline updated")
                return redirect(f"/todo/{page}/{sorter}")
            else:
                return redirect(f"/todo/{page}/{sorter}")
    else:
        #get list of dict from SQL 
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            if page != "completed":
                cur.execute("SELECT * from todos WHERE user_id = %s AND completed=false", [session["user_id"]])
                tasks = cur.fetchall()
                return render_template("todo.html", todos=render_todos(tasks, page), page=page, username=GET_USERNAME(), sorter=sorter, tags=session["user_tags"])
            else:
                cur.execute("SELECT * from todos WHERE user_id=%s AND completed=true", [session["user_id"]])
                tasks = cur.fetchall()
                todos = render_todos(tasks, "task")
                todos.extend(render_todos(tasks, "expired"))
                return render_template("todo.html", todos=todos, page="completed", username=GET_USERNAME(), sorter=sorter, tags=session["user_tags"])


@app.route("/todo/create/<sorter>", methods=["GET","POST"])
@login_required
def create(sorter):
    if request.method == "POST":
        start = request.form.get("start")
        deadline = request.form.get("deadline")
        title = request.form.get("title")
        tag = request.form.get("tag")
        descriptor = request.form.get("descriptor")

        tag = tag if tag else request.form.get("custom_tag").strip().lower()
        start = start if start else request.form.get("start-mobile")
        deadline = deadline if deadline else request.form.get("deadline-mobile")
        if not title or not tag or not start or not deadline:
            return apology("MISSING FIELD(s)")

        d_deadline = date.fromisoformat(deadline)
        d_start = date.fromisoformat(start)
        
        if (d_deadline-d_start).days < 0:
            return apology("DEADLINE PRECEDES START DATE")
        elif (d_deadline-date.today()).days < 0:
            return apology("DEADLINE MUST BE A FUTURE DATE")
        with conn:
            cur = conn.cursor()
            cur.execute("INSERT INTO todos (user_id, title, descriptor, start, deadline, tag) values (%s, %s, %s, %s, %s, %s)",\
                    (session["user_id"], title, descriptor, start, deadline, tag))
            if tag not in session["user_tags"]:
                cur.execute("INSERT INTO tags (user_id, tag) VALUES (%s, %s)",\
                        (session["user_id"], tag))
                session["user_tags"].append(tag)
            conn.commit()
            flash("Successfully added new task")
        return redirect("/todo/task/deadline")
    else:
        return render_template("todo.html", page="create", username=GET_USERNAME(), sorter=sorter, tags=session["user_tags"])

@app.route("/timetable")
@login_required
def timetable():
    return redirect("/timetable/table")

@app.route("/timetable/table", methods=["GET", "POST"])
@login_required
def table_display():
    if request.method=="POST":
        delete_id = request.form.get("delete")
        with conn:
            cur = conn.cursor()
            cur.execute("DELETE FROM timetables WHERE id = %s", [delete_id])
            flash("Task deleted")
        return redirect("/timetable/table")
    else:
        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("SELECT * from timetables WHERE user_id = %s", [session["user_id"]])
            tasks = cur.fetchall()

            cur.execute("SELECT distinct post_date from timetables WHERE user_id = %s", [session["user_id"]])
            dates = cur.fetchall()
            for i in range(len(dates)):
                dates[i] = str(dates[i][0])
            return render_template("timetable.html", page="table", username=GET_USERNAME(), tags=session["user_tags"], timetable=render_timetable(tasks), dates=dates)

@app.route("/timetable/create", methods=["GET", "POST"])
@login_required
def table_create():
    if request.method == "POST":
        today = str(date.today())
        starttime = request.form.get("starttime")
        endtime = request.form.get("endtime")
        title = request.form.get("title")
        tag = request.form.get("tag")
        descriptor = request.form.get("descriptor")

        if not starttime or not endtime:
            starttime = request.form.get("starttime-mobile")
            endtime = request.form.get("endtime-mobile")

        tag = tag if tag else request.form.get("custom_tag").strip().lower()
        if not title or not tag or not starttime or not endtime:
            return apology("MISSING FIELD(s)")

        td_start = datetime.fromisoformat(f'{today} {starttime}')
        td_end = datetime.fromisoformat(f'{today} {endtime}')
        if '-' in str(td_end-td_start) or td_start == td_end:
            return apology("END TIME PRECEDES, OR IDENTICAL TO START TIME")

        with conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("SELECT start_at, end_at FROM timetables WHERE user_id = %s AND post_date = %s", [session["user_id"], today])
            times = cur.fetchall()
            for item in times:
                if check_overlap(item["start_at"], item["end_at"], starttime) or \
                check_overlap(item["start_at"], item["end_at"], endtime): # overlap == true then return
                    return apology("OVERLAP IN TIMETABLE")

            cur.execute("INSERT INTO timetables (user_id, title, descriptor, start_at, end_at, tag, post_date) values (%s, %s, %s, %s, %s, %s, %s)",\
                    (session["user_id"], title, descriptor, starttime, endtime, tag, today))
            if tag not in session["user_tags"]:
                cur.execute("INSERT INTO tags (user_id, tag) VALUES (%s, %s)",\
                        (session["user_id"], tag))
                session["user_tags"].append(tag)
            conn.commit()
            flash("Successfully added new task")
            return redirect("/timetable/table")
    else:
        return render_template("timetable.html", page="create", username=GET_USERNAME(), tags=session["user_tags"])

@app.route("/note")
@login_required
def note():
    return redirect("/note/new/")

@app.route("/note/new/")
@login_required
def new_note():
    return render_template("note.html", page="new", username=GET_USERNAME())

@app.route("/note/view/")
@login_required
def view_note():
    return render_template("note.html", page="view", username=GET_USERNAME())

@app.route('/sign_s3_post/')
@login_required
def sign_s3_post():
  file_name = request.args.get('file_name')
  file_type = request.args.get('file_type')
  file_name = f'{session["user_name"]}/{file_name}'  # to prevent users arbituarily uploading to others' account
  presigned_post = s3.generate_presigned_post(
    Bucket = S3_BUCKET,
    Key = file_name,
    Fields = {"Content-Type": "plain/text"},
    Conditions = [{"Content-Type": file_type}],
    ExpiresIn = 3600
  )
  return json.dumps({
    'data': presigned_post,
    'url': 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, file_name)
  })

@app.route('/get_download_link/<user>/<cat>/<tag>/<filename>')
@login_required
def get_s3_dl(user, cat, tag, filename):
    if tag == "" or cat == "" or filename == "":
        return "error", 404
    else:
        return s3.generate_presigned_url(
                ClientMethod="get_object",
                Params={
                    'Bucket': S3_BUCKET,
                    'Key':f'{user}/{cat}/{tag}/{filename}'
                    }
                )

@app.route('/delete_note/', methods=["POST"])
@login_required
def delete_note():
    path = request.form.get("note-choice")
    if not path:
        return redirect("/note/view")
    s3.delete_object(Bucket=S3_BUCKET, Key=path.replace('+', ' '))
    return redirect("/note/view/")

@app.route('/user_note_list/')
@login_required
def get_note_list():
    user_name = session["user_name"]
    notes_list = s3.list_objects(
    Bucket = S3_BUCKET,
    Delimiter='string',
    EncodingType='url',
    MaxKeys = 100,
    Prefix = user_name,
    )
    note_list_formatted = []
    categories = []
    tags = []
    paths = []
    
    if "Contents" not in notes_list.keys():
        return "Cannot retrieve note list", 404
    else:
        for note in notes_list["Contents"]:
            note_list_formatted.append(note["Key"])
        for note in note_list_formatted:
            paths.append(\
                    f"{note.split('/')[1].replace('+', ' ')}/{note.split('/')[2].replace('+', ' ')}")
        for note in paths:
            categories.append(note.split('/')[0])
            tags.append(note.split('/')[1])
        return {'notes' : note_list_formatted, 'paths': sorted(filter_duplicate(paths)), 'categories': filter_duplicate(categories), 'tags': filter_duplicate(tags)}

@app.route("/get_user_key/")
@login_required
def get_dek():
    return b64encode(session["dek"])

@app.route("/error")
def apology(message):
    return render_template("apology.html", error_message=message, username=GET_USERNAME())
