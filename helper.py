from datetime import date, datetime
from time import time
from functools import wraps
from flask import session, redirect, url_for, request
from Crypto import Random, Protocol, Cipher, Util, Hash
from Crypto.Cipher import AES
from base64 import b64encode, b64decode
today = date.today()
# colors to integrate with bulma framework
colors = {"red": "danger",
        "blue": "info",
        "green": "success",
        "yellow": "warning",
        }

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect(url_for("login", next=request.url.split("/")[3]))
        return f(*args, **kwargs)
    return decorated_function

def render_todos(todos, page):
    todos_task = []
    todos_expired = []
    # NOT IMPLEMENTED: tag colors - because they look ugly
    tag_colors = {
            "life": colors["blue"],
            "household": colors["green"],
            "learning": colors["yellow"],
            "medical": colors["red"],
            }
    for task in todos:
        # Calculate the remaining and total days for each task
        remain_day = get_remain( today, task["deadline"])
        total_day = get_remain( task["start"],task["deadline"])
        # Skip task if expired
        if remain_day < 0 or total_day < 0:
            remain_percent = 100
            progress_color = colors["blue"]
            append_task(todos_expired, task, remain_day, total_day, remain_percent, progress_color)
        else:
            # Divide the days to get a remain percent
            if total_day != 0:
                remain_percent = int(remain_day / total_day * 100)
            else:
                remain_percent = 100
            # Get color for progress bar
            if remain_percent >= 66:
                progress_color = colors["blue"]
            elif remain_percent < 15:
                progress_color = colors["red"]
            elif remain_percent < 33:
                progress_color = colors["yellow"]
            else:
                progress_color = colors["green"]
            append_task(todos_task, task, remain_day, total_day, remain_percent, progress_color)
    if page == "task":
        return todos_task
    elif page == "expired":
        return todos_expired

def append_task(todos, task, remain_day, total_day, remain_percent, progress_color):
    todos.append({
         "id":task["id"],
         "title":task["title"],
         "tag":task["tag"],
         "descriptor": task["descriptor"],
         "start":task["start"],
         "deadline": task["deadline"],
         "remain_day": remain_day,
         "total_day": total_day,
         "remain_percent": remain_percent,
         "progress_color": progress_color,
         # "tag_color": tag_colors[task["tag"]],
         })
    return todos

# function to get remain_day
def get_remain(current, end):
    return (end - current).days

def get_deadline(tasks):
    return tasks.get("deadline")
def get_start(tasks):
    return tasks.get("start")
def get_remain_in_list(tasks):
    return (date.fromisoformat(tasks.get("deadline")) - date.fromisoformat(tasks.get("start"))).days

def render_time(time_obj):
    return f"{time_obj.hour:02}:{time_obj.minute:02}"

def get_duration(t1, t2, date): # t1 and t2 must be time obj
    td_start = datetime.fromisoformat(f'{date} {t1}')
    td_end = datetime.fromisoformat(f'{date} {t2}')
    duration = str(td_end - td_start).split(':')
    return f'{duration[0]}H{duration[1]}M'
    

def render_timetable(timetable_obj):

    for i in range(len(timetable_obj)):
        # user_id, title, descriptor, start_at, end_at, tag, post_date
        timetable_obj[i] = dict(timetable_obj[i])
        start_at = timetable_obj[i]["start_at"]
        end_at = timetable_obj[i]["end_at"]
        post_date = timetable_obj[i]["post_date"]
        timetable_obj[i].update({"start_time" : render_time(start_at), "end_time" : render_time(end_at),\
            "duration" : get_duration(start_at, end_at, post_date)})

    return sorted(timetable_obj, key=lambda r:r["start_at"])

def check_overlap(start_at, end_at, time_to_be_checked):
    t1 = dt_constructor(start_at)
    t2 = dt_constructor(end_at)
    checked = dt_constructor(time_to_be_checked)
    if (checked > t1) and (checked < t2): # is the checked time AFTER start AND BEFORE end? 
        return True                       # thus (AFTER start and end) OR (BEFORE start and end) are both excluded
                                          # also, BEFORE start and AFTER end is impossible
    else:
        return False

def dt_constructor(time):
    return datetime.fromisoformat(f'0001-01-01 {time}')

def set_default(default_BM):
    insert_set = []
    for category in default_BM:
        for bookmark in category["bookmarks"]:
            insert_set.append([bookmark["title"], bookmark["url"], "default", category["name"]])
        for folder in category["folders"]:
            for bookmark in folder["bookmarks"]:
                insert_set.append([bookmark["title"], bookmark["url"], folder["name"], category["name"]])
    return insert_set

def new_cat(name, folder_name, bookmark):
    CATEGORY_PROTOTYPE = {'name' : '', 'folders': [], 'bookmarks': []}
    new = CATEGORY_PROTOTYPE.copy()
    new['name'] = name
    new_row = new_bookmark(bookmark)

    if folder_name == "default":
        new['bookmarks'].append(new_row)
    else:
        new['folders'].append(new_fold(folder_name, new_row))
    return new

def new_fold(name, bookmark):
    FOLDER_PROTOTYPE = {'name': '', 'bookmarks': []}
    new = FOLDER_PROTOTYPE.copy()
    new['name'] = name
    new['bookmarks'].append(new_bookmark(bookmark))
    return new

def new_bookmark(bookmark):
    BOOKMARK_PROTOTYPE = {'title': '', 'id': '', 'url': ''}
    new = BOOKMARK_PROTOTYPE.copy()
    new['title'] = bookmark["title"]
    new['id'] = bookmark["id"]
    new['url'] = bookmark["url"]
    return new

def render_bookmark(bookmark_list):
    category_names = []   # lists to track uninserted cats and folds
    folder_names = []     # should be a tuple
    rendered_list = []   # list to be returned to be displayed

    for bookmark in bookmark_list:
        if bookmark["category"] not in category_names:   # if a cat is new - call the new_cat() fx to create it
            category_names.append(bookmark["category"])
            rendered_list.append(new_cat(bookmark["category"], bookmark["folder"], bookmark)) 
            if (bookmark["folder"], bookmark["category"]) not in folder_names:
                folder_names.append((bookmark["folder"], bookmark["category"]))
            # in new_cat() is the logic to handle both default and non-default folders
        else:
            for category in rendered_list:              # if a category is NOT new - then we first need to select it 
                if category["name"] == bookmark["category"]:
                    if bookmark["folder"] == "default": # then it could either be 'default'
                        category["bookmarks"].append(new_bookmark(bookmark))
                    elif (bookmark["folder"], bookmark["category"]) not in folder_names:  # or non-default new folder
                        folder_names.append((bookmark["folder"], bookmark["category"]))
                        category["folders"].append(new_fold(bookmark["folder"], bookmark))
                    else:
                        for folder in category["folders"]:       # or non-default existing folder, which we have to select it
                            if folder["name"] == bookmark["folder"]:
                                folder["bookmarks"].append(new_bookmark(bookmark))
    return rendered_list

def filter_duplicate(lists):
    res = []
    for item in lists:
        if item not in res:
            res.append(item)
    return res

def generate_new_edek(DEK, password, salt, nonce): #remember to pass bytes 
    KEK = Protocol.KDF.PBKDF2(password, salt, 32, count=100000, hmac_hash_module=Hash.SHA256)
    cipher = AES.new(KEK, AES.MODE_CCM, nonce=nonce)
    EDEK = b64encode(cipher.encrypt(DEK)).decode('utf-8')
    EDEK_string = f'{b64encode(salt).decode("utf-8")}-{b64encode(nonce).decode("utf-8")}-{EDEK}'
    return EDEK_string

def decrypt_edek(salt, nonce, edek, password): #all bytes except password
    KEK = Protocol.KDF.PBKDF2(password, salt, 32, count=100000, hmac_hash_module=Hash.SHA256)
    cipher = AES.new(KEK, AES.MODE_CCM, nonce=nonce)
    return cipher.decrypt(edek)

